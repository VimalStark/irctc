package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login {

	public ChromeDriver driver;
	@Given("Launch the browser")
	
	public void launchTheBrowser() {
	    
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
	   
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
	  
		driver.manage().window().maximize();

	}

	@Given("set timeouts")
	public void setTimeouts() {
	   
	  driver.manage().timeouts().implicitlyWait(2000,TimeUnit.SECONDS);
	}

	@Given("load the url")
	public void loadTheUrl() {
	   
		driver.get("http://leaftaps.com/opentaps");

	}

	@Given("Enter user as Demosalesmanager")
	public void enterUserAsDemosalesmanager() {
	   
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		
	}

	@Given("Enter password as crmsfa")
	public void enterPasswordAsCrmsfa() {
	   
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@When("clicking login button")
	public void clickingLoginButton() {
	 
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("verify login is success")
	public void verifyLoginIsSuccess() {
	
	    System.out.println("verify login is success");
	}

	
	@Then("input CRM\\/SFA")
	public void inputCRMSFA() {
	    // Write code here that turns the phrase above into concrete actions
	 driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("click create lead")
	public void clickCreateLead() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Create Lead").click();
	}

	@Then("input companyname")
	public void inputCompanyname() {
	    // Write code here that turns the phrase above into concrete actions
	
	}

	@Then("firstname")
	public void firstname() {
	    // Write code here that turns the phrase above into concrete actions

	}

	@Then("lastname")
	public void lastname() {
	    // Write code here that turns the phrase above into concrete actions
	 
	}

	@Then("click create")
	public void clickCreate() {
	    // Write code here that turns the phrase above into concrete actions
	
	}



	
	
}
