package org.testleaf.leaftaps.seleniumbase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjectSpecificMethods {
	
     public ChromeDriver driver;
     @Parameters({"url","username","password"})
     @BeforeMethod
	public  void login (String url,String username,String password)
	{
		// TODO Auto-generated method stub
		
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver/chromedriver.exe");
		 driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys(username);
		driver.findElementById("password").sendKeys(password, Keys.ENTER);
		
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();

	
	}
     @AfterMethod
     public  void logout ()
 	{
    	 driver.close();
 	}
     
     
}
