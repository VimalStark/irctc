package org.testleaf.leaftaps.seleniumbase;


import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Create extends ProjectSpecificMethods {
	@Test(dataProvider="fetchdata")
	
		// TODO Auto-generated method stub
	
			public void runCreateLead(String url,String username,String password ) {
				driver.findElementByLinkText("Leads").click();
				driver.findElementByLinkText("Create Lead").click();
				driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
				driver.findElementById("createLeadForm_firstName").sendKeys("vimal");
				driver.findElementById("createLeadForm_lastName").sendKeys("M");
				driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("74");
				driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8939350809");
				driver.findElementByName("submitButton").click();
		}
	
	
	@DataProvider(name="fetchdata")
	 public String[][] getdata(){
		 String[][] data = new String [2][3];
		 data [0][0]="Testleaf";
		 data [0][1]="vimal";
		 data [0][2]="M";
		 
		 data [1][0]="CTS";
		 data [1][1]="vimals";
		 data [1][2]="Ms";
		 
		 
		return data;
		 
		 
	 }
	

		}
		
	
