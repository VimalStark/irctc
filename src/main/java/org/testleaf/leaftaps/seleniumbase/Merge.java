    package org.testleaf.leaftaps.seleniumbase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Merge extends ProjectSpecificMethods 
{
@Test
	public  void merger()
	{
	
		/*System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("demosalesmanager");
		driver.findElementById("password").sendKeys("crmsfa", Keys.ENTER);
		
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();*/
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		//move to new window
		Set<String> allwindows1 = driver.getWindowHandles();      
		List<String>listwindows1 = new ArrayList<String>(allwindows1);
		driver.switchTo().window(listwindows1.get(1));
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10138");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//a[text()='10138']").click();
		
		//move to primary window
		driver.switchTo().window(listwindows1.get(0));
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		
		//move to new window
		Set<String> allwindows2 = driver.getWindowHandles();
		List<String>listwindows2 = new ArrayList(allwindows2);
		driver.switchTo().window(listwindows2.get(1));
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10138");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//a[text()='10138']").click();
		
		//move to primary window
		driver.switchTo().window(listwindows2.get(0));
		driver.findElementByXPath("//a[text()='Merge']").click();
		
		Alert alert = driver.switchTo().alert();
		String text = alert.getText();
		System.out.println(text);
		alert.accept();
		
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
		
		
	}

}
