package org.testleaf.leaftaps.seleniumbase;
	

		import java.util.Set;
		import java.util.concurrent.TimeUnit;

		import org.openqa.selenium.By;
		import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

		public class Merge1 extends  ProjectSpecificMethods {
@Test
		public  void merge2() throws InterruptedException {
	
		// TODO Auto-generated method stub
		/*System.setProperty("webdriver.chrome.driver", "./Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElementByLinkText("CRM/SFA").click();*/
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//a[text() = 'Merge Leads']").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		String parentHandle = driver.getWindowHandle();
		Set<String> windowHandles = driver.getWindowHandles();
		for (String handles : windowHandles) {
		if(!handles.equals(parentHandle))
		{
		driver.switchTo().window(handles);
		}
		}
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@class = ' x-form-text x-form-field']").sendKeys("10032");
		driver.findElementByClassName("x-btn-center").click();
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(parentHandle);
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Set<String> windowHandles1 = driver.getWindowHandles();
		for (String handles : windowHandles1) {
		if(!handles.equals(parentHandle))
		{
		driver.switchTo().window(handles);
		}
		}
		driver.manage().window().maximize();
		driver.findElementByName("id").sendKeys("10033");
		driver.findElementByClassName("x-btn-center").click();
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(parentHandle);
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		driver.findElementByName("id").sendKeys("10039");
		driver.findElementByClassName("x-btn-center").click();
		String text = driver.findElementByClassName("x-paging-info").getText();
		System.out.println(text);

	}

}
