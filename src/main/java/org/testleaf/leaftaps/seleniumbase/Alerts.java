package org.testleaf.leaftaps.seleniumbase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alerts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver/chromedriver.exe");
		
		//launch chrome
ChromeDriver driver = new ChromeDriver();
driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
driver.switchTo().frame("iframeResult");
driver.findElement(By.xpath("//button[@onclick='myFunction()']")).click();
Alert Harry = driver.switchTo().alert();
Harry.sendKeys("Potter");
Harry.accept();
WebElement Verify = driver.findElement(By.id("Hello Harry! How are you today?"));
Verify.isDisplayed();
System.out.println(Verify.isDisplayed());

	
}
	
}





