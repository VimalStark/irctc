package browser;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
System.setProperty("webdriver.chrome.driver","./drivers/chromedriver/chromedriver.exe");
		
		//launch chrome
ChromeDriver driver = new ChromeDriver();
driver.get("https://www.irctc.co.in/eticketing/userSignup.jsf");
driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

Set<String> allwindows = driver.getWindowHandles();
List <String> listwindows= new ArrayList<String>(allwindows);
driver.switchTo().window(listwindows.get(1));
System.out.println(driver.getTitle());





	}

}
