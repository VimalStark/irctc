package browser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Select3elements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//launch chrome
		

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/selectable.html");
		driver.manage().window().maximize();
		WebElement element1 = driver.findElementByXPath("(//li[@class='ui-widget-content ui-selectee'])[1]");
		WebElement element2 = driver.findElementByXPath("(//li[contains(@class,'ui-widget-content ui-selectee')])[2]");
		WebElement element3 = driver.findElementByXPath("(//li[contains(@class,'ui-widget-content ui-selectee')])[3]");
		Actions builder=new Actions(driver);
		builder.clickAndHold(element1).clickAndHold(element2).clickAndHold(element3).release().perform();
		driver.close();
		
	}

}
