package browser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Dragdrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//launch chrome
		

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/drag.html");
		driver.manage().window().maximize();
		WebElement tester = driver.findElementById("draggable");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(tester, 100, 100).perform();
		
		
	}

}
